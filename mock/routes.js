const express = require('express');
const router = express.Router();
const candidatesController = require('./controllers/candidates');
const authController = require('./controllers/auth');


router.get('/candidates', candidatesController.getCandidates);
router.put('/candidates', candidatesController.updateCandidates);
router.post('/login', authController.login);
router.post('/logout', authController.logout);

router.get('/event-candidates', candidatesController.getEventCandidates);


module.exports = router;
