const mockActiveCandidates = require('../data/active_candidates.json');
const mockPastCandidates = require('../data/past_candidates.json');
const mockEventCandidates = require('../data/event_candidates.json');

exports.getCandidates = (req, res) => {
    const isActive = req.query.isActive;
    if(isActive === 'true'){
        return res.json(mockActiveCandidates);
    }
    return res.json(mockPastCandidates);
};

exports.updateCandidates = (req, res) => {
    const candidateListToLock = req.body.candidateIds;
    if(req.body.isLocked === true || req.body.isLocked === false){
        const toBeLocked = !!req.body.isLocked;
        mockActiveCandidates.forEach(cand => {
            if(candidateListToLock && candidateListToLock.includes(cand.candidateId)){
                cand.isLocked = toBeLocked;
            }
        });
    }    
    return res.status(200).send({status: 'success'});
};

exports.deactivateCandidate = (req, res) => {
    return res.status(200).send({status: 'success'});
};

exports.getEventCandidates = (req, res) => {
    console.log('comes here');
    return res.json(mockEventCandidates);
};