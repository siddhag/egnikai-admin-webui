import { logout } from './auth';

const apiUrl = process.env.REACT_APP_API_URL || 'http://localhost:8080/';

const apiRequest = params =>{
    const baseApiUrl = `${apiUrl}admin/api/${params.url}`;
    const options = {
        ...params.options,
        credentials: 'include',
        headers: {
            'content-type': 'application/json',
            ...params.options && params.options.headers
        }
    };
    return fetch(baseApiUrl, options)
            .then((response) => {
            if (response.status === 401) {
                logout();
                return Promise.reject({
                    errors: 'Unauthorised'
                });
            }
            const isJSONResponse = response.headers.get('content-type') && response.headers.get('content-type').includes('json');
            const responsePromise = isJSONResponse ? response.json() : response.text();
            return responsePromise.then((result) => {
                if (response.ok) {
                return result;
                }
                return Promise.reject(result);
            });
            })
            .catch(errors => (Promise.reject({errors})))
};

const delay = ms => new Promise(res => setTimeout(res, ms));


const setPromiseState = (isPending = false, isFulfilled = false, isRejected = false) => ({
    isPending,
    isFulfilled,
    isRejected
});

export {
    apiRequest, setPromiseState, delay,

};