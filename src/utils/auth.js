import EventEmitter from '../EventBus';
export const isLoggedIn  = () => {
    return JSON.parse(sessionStorage.getItem("isLoggedIn"));
};

export const onLoginSuccess = () => {
    sessionStorage.setItem("isLoggedIn", true);
    EventEmitter.emit('userLoggedIn');
};

export const logout = () => {
    sessionStorage.setItem("isLoggedIn", false);
    EventEmitter.emit('userLoggedOut');
};