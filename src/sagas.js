import challengeSaga from './pages/challenge/challengeSagas'
import createCandidateSagas from './pages/candidate/CreateCandidateSagas'
export default [
    challengeSaga,
    createCandidateSagas
]