export default {
    activeCandidates: 'candidates?isActive=true',
    pastCandidates: 'candidates?isActive=false',
    lockorunlock: 'candidates',
    deactivate: 'candidates',
    login: 'login',
    logout: 'logout',
    eventCandidates: 'candidates/create',
    challenges: 'challenges'
}