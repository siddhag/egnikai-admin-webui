import React from 'react';

class CandidateTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            challenges: props.challenges,
            optionKey:0
        };
    }


    handleChange(index, value) {
        this.state.data[index].challengeId = value;
        this.props.callback(this.state.data);
    }

    challengeOptions() {
        return this.state.challenges.map((item) =>
            <option key={this.state.optionKey} value={item.challengeId}>{item.challengeTitle}</option>
        );
    }


    render() {
        return (
            <div className="row" id="create-candidate">
                <div className="col" id="candidate-list">
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th>Serial No. </th>
                            <th>Candidate Name</th>
                            <th>Challenge</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.data.map((row, index) => {
                            return (
                                <tr className='assign-challenge' key={index}>
                                    <td className="align-middle">
                                        {index+1}
                                    </td>
                                    <td>
                                        {this.state.data[index].candidateName}
                                    </td>
                                    <td>
                                        <select value={this.state.data[index].challengeId || ''}
                                                onChange={(e) => this.handleChange(index, e.target.value)}>
                                            {<option value="">Select challenge</option>}
                                            {this.challengeOptions()}
                                        </select>
                                    </td>
                                </tr>
                            );
                        })}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default CandidateTable;


