import React from 'react';
import PropTypes from 'prop-types';


const PastSessionList = ({
    candidates
}) => {
    const rows = candidates.map((candidate, index) => {
        return (
            <tr key={index}>
                <td className="align-middle">{candidate.candidateId}</td>
                <td className="align-middle">{candidate.candidateName}</td>
                <td className="align-middle">{candidate.score}</td>
                <td className="align-middle">
                    {candidate.candidateGitBranch ? (<a target="_blank" href={candidate.candidateGitBranch}>
                        Git Repo
                    </a>): null }    
                </td>
                <td>
                    <button
                        className="btn btn-info"
                        >
                        View Details
                    </button>
                </td>
            </tr>
        );
    });
    return (<React.Fragment>            
        <table className="table table-striped table-sm">
            <thead>
                <tr>
                    <th>CID</th>
                    <th>Candidate Name</th>
                    <th>Score</th>
                    <th>Git</th>
                    <th/>
                </tr>
            </thead>
            <tbody>
                {rows}
            </tbody>
        </table>
    </React.Fragment>)
}

PastSessionList.propTypes = {
    candidates: PropTypes.array
};
PastSessionList.defaultProps = {
    candidates: []
};

export default PastSessionList;