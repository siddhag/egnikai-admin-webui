import React from 'react';
import PropTypes from 'prop-types';

const SessionList = ({
            candidates,
            isAllSelected,
            toggleSelectAllCheckChange,
            toggleCandidateSelection,
            lockCandidate,
            unlockCandidate,
            deactivateCandidate
        }) => {
        const onChange = (e) => {
            toggleSelectAllCheckChange(e.target.checked);
        };

        const onItemCheckChange = (cid) => {
            toggleCandidateSelection(cid);
        };

        const onUnlockClick = (cid) => {
            lockCandidate(cid);
        };

        const onLockClick = (cid) => {
            unlockCandidate(cid);
        };

        const onDeactivateClick = (cid) => {
            deactivateCandidate(cid);
        };

        const rows = candidates.map((candidate, index) => {
            return (
                <tr key={index}>
                    <td className="align-middle">
                        <input type="checkbox" 
                            checked={candidate.selected}
                            onChange={onItemCheckChange.bind(null, candidate.candidateId)}
                        />
                    </td>
                    <td className="align-middle">{candidate.candidateId}</td>
                    <td className="align-middle">{candidate.userId}</td>
                    <td className="align-middle">{candidate.eventId}</td>
                    <td className="align-middle">{candidate.candidateName}</td>
                    <td className="align-middle">{candidate.score}</td>
                    <td className="align-middle">
                        {candidate.candidateGitBranch ? (<a target="_blank" href={candidate.candidateGitBranch}>
                            Git Repo
                        </a>): null }
                    </td>
                    <td className="align-middle">
                        {candidate.isLocked? (
                            <i 
                                onClick={onLockClick.bind(null, candidate.candidateId)}
                                className="fa fa-lock"
                            ></i>
                            ) : 
                            (<i
                                onClick={onUnlockClick.bind(null, candidate.candidateId)} 
                                className="fa fa-unlock" />)
                            }
                    </td>
                    <td>
                        <button
                            onClick={onDeactivateClick.bind(null, candidate.candidateId)}
                            className="btn btn-danger"
                            >
                            Deactivate
                        </button>
                    </td>
                </tr>
            );
        });
        return (<React.Fragment>            
            <table className="table table-striped table-sm">
                <thead>
                    <tr>
                        <th>
                            <input 
                                type="checkbox" 
                                checked={isAllSelected}
                                onChange={onChange}
                                />
                        </th>
                        <th>CID</th>
                        <th>User Id</th>
                        <th>Event Name</th>
                        <th>Candidate Name</th>
                        <th>Score</th>
                        <th>Git</th>
                        <th />
                        <th />
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        </React.Fragment>)      
}

SessionList.propTypes = {
    candidates: PropTypes.array,
    isAllSelected: PropTypes.bool,
    toggleSelectAllCheckChange: PropTypes.func,
    lockCandidate: PropTypes.func,
    unlockCandidate: PropTypes.func,
    deactivateCandidate: PropTypes.func
};

SessionList.defaultProps = {
    candidates: [],
    isAllSelected: false
};

export default SessionList;