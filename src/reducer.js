import {combineReducers} from 'redux';
import challengeReducer from './pages/challenge/challengeReducer';
import createCandidateReducer from './pages/candidate/CreateCandidateReducer';

export default combineReducers({
    challenges: challengeReducer,
    candidates: createCandidateReducer
});