import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import { apiRequest } from '../../utils/api';
import { onLoginSuccess } from '../../utils/auth';
import apiConstants from '../../constants/api';
import './Login.css';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      showError: false
    };
    this.setUsername = this.setUsername.bind(this);
    this.setPassword = this.setPassword.bind(this);
    this.isDisabled = this.isDisabled.bind(this);
  }
  
  setUsername(event) {
    this.setState({
      username: event.target.value
    });
  }

  setPassword(event) {
    this.setState({
      password: event.target.value
    });
  }

  isDisabled() {
    const {
      username,
      password
    } = this.state;
    return (username.length === 0 || password.length === 0);
  }

  mayBeRenderError() {
    const {showError} = this.state;
    if (showError) {
      return (
        <div className="text-warning">
          *Invalid username or password
        </div>
      );
    }
    return null;
  }

  onLoginFailure = () => {
    this.setState({
      showError: true
    });
  }

  loginRequest = ({username, password}) => {
    const delimitedCredentials = `${username}:${password}`;
    this.setState({
      showError: false
    });
    apiRequest({
      url: apiConstants.login,
      options: {
        method: 'POST',
        credentials: 'include',
        headers: {
          Authorization: `Basic ${btoa(delimitedCredentials)}`,
          'Content-Type': 'application/json'
        }
      }
    }).then(onLoginSuccess, this.onLoginFailure);
  }

  render() {
    const {from} = this.props.location.state || {from: {pathname: '/home'}};
    if (this.props.isLoggedIn) {
      return (
        <Redirect to={from} />
      );
    }
    return (
      <section className="login row">
        <form
          className="align-middle col-md-4 offset-md-3"
          onSubmit={(event) => {
            event.preventDefault();
            const {
              username,
              password
            } = this.state;
            this.loginRequest({username, password});
          }}
        >
          <div>
            <article>
              <div>
                <h1 className="h3 mb-3 font-weight-normal">
                  Login
                </h1>
                {this.mayBeRenderError()}
                <div className="form-group">
                  <label 
                    htmlFor="username"
                    className="sr-only"
                  >
                  Username
                  </label>
                  <input
                    onChange={this.setUsername}
                    id="username"
                    className="form-control"
                    placeholder="Username"
                    type="text"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="password" className="sr-only">Password</label>
                  <input
                    onChange={this.setPassword}
                    id="password"
                    type="password"
                    placeholder="Password"
                    className="form-control"
                  />
                </div>
                <div className="control">
                  <button
                    disabled={this.isDisabled()}
                    className="btn btn-success"
                  >
                    Login
                  </button>
                </div>
              </div>
            </article>
          </div>
        </form>
      </section>
    );
  }
}

LoginForm.propTypes = {
  loginRequest: PropTypes.func,
  showError: PropTypes.bool,
  isLoggedIn: PropTypes.bool,
  isPromisePending: PropTypes.bool,
  location: PropTypes.object,
  reset: PropTypes.func
};

LoginForm.defaultProps = {
  loginRequest: null,
  showError: false,
  isLoggedIn: false,
  isPromisePending: false,
  location: {},
  reset: null
};

export default LoginForm;
