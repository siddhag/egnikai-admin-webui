import React, { Component } from 'react';
import SessionList from '../../components/session-list/SessionList';
import { apiRequest } from '../../utils/api';
import apiConstants from '../../constants/api';
import './Home.css';
import {CSVLink, CSVDownload} from 'react-csv';

class Home extends Component {

    state = {
        candidates: [],
        isAllSelected: false

    }
    componentWillMount(){
        this.getCandidates();
    }
    getCandidates = () => {
        apiRequest({
            url: apiConstants.activeCandidates
        })
        .then(this.deselectAllCandidates);
    }
    lockSelectedCandidates = () => {
        const selectedCandidates = this.state.candidates.filter(c => c.selected);
        if(selectedCandidates.length > 0) {
            this.requestToLockCandidates(selectedCandidates.map(c => c.candidateId));
        }        
    }

    unlockSelectedCandidates = () => {
        const selectedCandidates = this.state.candidates.filter(c => c.selected);
        if(selectedCandidates.length > 0) {
            this.requestToUnlockCandidates(selectedCandidates.map(c => c.candidateId));
        }
    };

    lockCandidate = (candidateId) => {
        this.requestToLockCandidates([candidateId]);
    }

    unlockCandidate = (candidateId) => {
        this.requestToUnlockCandidates([candidateId]);
    }

    deactivateCandidate = (candidateId) => {
        const reqBody = {
            isActive: false,
            candidateIds: [candidateId]
        };
        apiRequest({
            url: apiConstants.deactivate,
            options: {
                method: 'PUT',
                body: JSON.stringify(reqBody)
            }
        })
        .then(this.getCandidates);
    }

    requestToUnlockCandidates = (candidateIds) => {
        const reqBody = {
            isLocked: false,
            candidateIds
        };
        apiRequest({
            url: apiConstants.lockorunlock,
            options: {
                method: 'PUT',
                body: JSON.stringify(reqBody)
            }
        })
        .then(this.getCandidates);
    }

    requestToLockCandidates = (candidateIds) => {
        const reqBody = {
            isLocked: true,
            candidateIds
        };
        apiRequest({
            url: apiConstants.lockorunlock,
            options: {
                method: 'PUT',
                body: JSON.stringify(reqBody)
            }
        })
        .then(this.getCandidates);
    }

    toggleCandidateSelection = (candidateId) => {
        const candidate = this.state.candidates.find(candidate => candidate.candidateId === candidateId);
        const updatedCandidates = this.state.candidates.map(currCandidate => {
            if(currCandidate.candidateId === candidate.candidateId){
                currCandidate.selected = !currCandidate.selected;
            }
            return currCandidate;
        });
        this.setState({
            candidates: updatedCandidates
        }, this.setIfAllSelected);

    }

    setIfAllSelected = () => {
        const isAllSelected = this.state.candidates.every(c => c.selected);
        this.setState({
            isAllSelected
        });
    }

    setSelection = select => candidates => {
        const candidatesToUpdate = candidates.map(candidate => {
            return {
                ...candidate,
                selected: select
            };
        });
        this.setState({
            candidates: candidatesToUpdate
        }, this.setIfAllSelected);
    }

    selectAllCandidates = (candidates) => {
        this.setSelection(true)(candidates);
    }

    deselectAllCandidates = (candidates) => {
        this.setSelection(false)(candidates);
    }

    toggleSelectAllCheckChange = (isChecked) => {
        isChecked ? 
            this.selectAllCandidates(this.state.candidates) :
            this.deselectAllCandidates(this.state.candidates);
    }

    filterDataForCSVFile = (candidates) => {
     return candidates.map(candidate => {
        return {
            eventId:candidate.eventId,
            candidateName:candidate.candidateName,
            userId:candidate.userId,
            password:candidate.password,
            challengeTitle:candidate.challengeTitle,
            createdAt:candidate.createdAt,
            score:candidate.score
        };
    })
    }



    render(){
    var textColor = {color:"white", textDecoration: "none"}
    var filterData = this.filterDataForCSVFile(this.state.candidates);
        return (
            <React.Fragment>
                <div className="row">
                    <div className="col">
                        <h2>Active User Sessions</h2>
                    </div>
                </div>
                <div className="row home_action">
                    <div className="col-2">
                        <button 
                            className="btn btn-warning"
                            onClick={this.lockSelectedCandidates}
                            >
                            Lock Candidate/s
                        </button>  
                    </div>
                    <div className="col-2">
                        <button 
                            className="btn btn-warning"
                            onClick={this.unlockSelectedCandidates}
                            >
                            Unlock Candidate/s
                        </button>  
                    </div>
                    <div className="col-8">
                        <button
                            className="btn btn-info pull-right"
                            >
                    <CSVLink data={filterData} filename={"Candidates-List"} style={textColor}>Download CSV file</CSVLink>
                        </button>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <SessionList 
                            candidates={this.state.candidates}
                            isAllSelected={this.state.isAllSelected}
                            toggleSelectAllCheckChange={this.toggleSelectAllCheckChange}
                            toggleCandidateSelection={this.toggleCandidateSelection}
                            lockCandidate={this.lockCandidate}
                            unlockCandidate={this.unlockCandidate}
                            deactivateCandidate={this.deactivateCandidate}
                        />
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Home;