import React, { Component } from 'react';
import { apiRequest } from '../../utils/api';
import PastSessionList from '../../components/past-session-list/PastSessionList';
import apiConstants from '../../constants/api';

class PastSession extends Component{
    state = {
        candidates: []
    }
    componentWillMount(){
        this.getCandidates();
    }
    getCandidates = () => {
        apiRequest({
            url: apiConstants.pastCandidates
        }).then((candidates) => {
            this.setState({
                candidates
            });
        });
    }
    render(){
        return (
            <React.Fragment>
                <div className="row">
                    <div className="col">
                        <h2>Past User Sessions</h2>
                    </div>                    
                </div>
                <div className="row">
                    <div className="col">
                        <PastSessionList candidates={this.state.candidates} />
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default PastSession;