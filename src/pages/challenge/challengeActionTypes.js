const challengeActionType = {
    FETCH_CHALLENGES: 'Fetch_challenges',
    CHALLENGE_FETCH: {
        pending: 'CHALLENGE_FETCH/pending',
        fulfilled: 'CHALLENGE_FETCH/fulfilled',
        rejected: 'CHALLENGE_FETCH/rejected'
    }
}
export default challengeActionType;
