import { fork, takeLatest, call, put} from  'redux-saga/effects';
import challengeActionTypes from './challengeActionTypes';
import {challengeFetchRejected, challengeFetchFulfilled, challengeFetchPending} from './challengeAction';
import {apiRequest} from '../../utils/api';


export function* getChallenges(action) {
    try {
        yield put((challengeFetchPending()));
        const result = yield call(apiRequest, {
            url: "challenges",
            options: {
                method: 'GET'
            }
        });
        yield put((challengeFetchFulfilled(result)));
    } catch (err) {

        yield put(challengeFetchRejected(err));
    }
}

export function* watchFetchChallenges() {
    yield takeLatest(challengeActionTypes.FETCH_CHALLENGES, getChallenges);
}

export default function* () {
    yield fork(watchFetchChallenges);
}