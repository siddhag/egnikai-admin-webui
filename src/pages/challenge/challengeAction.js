
import challengeActionTypes from './challengeActionTypes';
const fetchChallenges = () => (
    {
        type: challengeActionTypes.FETCH_CHALLENGES

    }
);

const challengeFetchPending = () => ({
    type: challengeActionTypes.CHALLENGE_FETCH.pending
});

const challengeFetchFulfilled = (data) => ({
    type: challengeActionTypes.CHALLENGE_FETCH.fulfilled,
    challenges:data
});

const challengeFetchRejected = () => ({
    type: challengeActionTypes.CHALLENGE_FETCH.rejected
});

export {
    fetchChallenges,
    challengeFetchPending,
    challengeFetchFulfilled,
    challengeFetchRejected
};
