import challengeActionType from './challengeActionTypes'
import {setPromiseState} from '../../utils/api'

const initialState= {
    isLoading: false,
    challenges: [],
    challengeFetchPromise: setPromiseState()
}

export default (state=initialState, action) => {
    switch (action.type){
        case challengeActionType.CHALLENGE_FETCH.pending:
          return {...state, challengeFetchPromise: setPromiseState(true, false, false)};
        case challengeActionType.CHALLENGE_FETCH.fulfilled:
          return {...state, challengeFetchPromise: setPromiseState(false, true, false), challenges: action.challenges};
        case challengeActionType.CHALLENGE_FETCH.rejected:
          return {...state, challengeFetchPromise: setPromiseState(false, false, true)};
        default:
            return state;
    }
}