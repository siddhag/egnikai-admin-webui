import React, {Component} from 'react';
import CSVReader from 'react-csv-reader';
import {Redirect} from 'react-router-dom';
import CandidateTable from "../../components/candidate-table/CandidateTable";

const VIEWS = {
    CREATE_EVENT_FORM: "CREATE_EVENT_FORM",
    CREATE_EVENT: "CREATE_EVENT",
    CANDIDATE_LIST: "CANDIDATE_LIST",
};
const columns = ['candidateName', 'challengeId'];

class CreateCandidate extends Component {
    state = {
        candidates: [],
        eventName: "",
        currentView: VIEWS.CREATE_EVENT_FORM,
        csvData: [],
        challenges:this.props.challenges || [],
        shouldRedirect: false,
        showError: false,
    }

    componentDidMount(){
        this.props.fetchChallenges();
    }

    componentDidUpdate(prevProps) {
        if (this.props.isRejected !== prevProps.isRejected) {
            this.setState({isRejected:this.props.isRejected})
        }
        if(this.props.isCreated !== prevProps.isCreated && this.props.isCreated){
            this.setState({shouldRedirect:true})
        }
      }

    componentWillUnmount(){
        this.setState({shouldRedirect:false})
    }

    onCSVSubmit = data => {
        data = data.filter(function(entry) { return entry.toString().trim() != ''; });
        this.setState({csvData: data});
        this.setState({candidates: this.mapCSVDataWithColumnNames()})
        this.setState({currentView: VIEWS.CREATE_EVENT});
    }

    setEventName(event) {
        this.setState({eventName: event.target.value})
    }

    mapCSVDataWithColumnNames() {
        var mappedData = [];
        this.state.csvData.forEach(row => {
            if (row === "")
                return;
            var hashMap = {};
            columns.forEach((column, index) => {
                Object.assign(hashMap, {[column]: row[index]});
            })
            mappedData.push(hashMap);
        })

        return mappedData;

    }

    renderCandidateCSVUploadForm = () => {
        return (
        <table>

        <tbody>
            <tr>
                        <td><label>Event Name</label></td>
                        <td><input type="text" className="rounded" value={this.eventName}
                               onChange={event => this.setEventName(event)}></input></td>
             </tr>
             <tr>
                <td>Upload Candidates data</td>
                <td><div className="upload-csv">
                    <CSVReader
                        cssClass="react-csv-input"
                        onFileLoaded={this.onCSVSubmit}
                    />
                </div>
                </td>
              </tr>
              </tbody>
                </table>
        )
    }

    renderCreateCandidateList = () => {
        const candidates = this.state.candidates;
        const challenges = candidates.map(c => c.challengeId);
        const enableSubmit = challenges.filter(c => !c).length == 0;
        const uniqueChallenge = challenges.filter(c => c).filter(this.onlyUnique).length == 1;
        return (
            <div>
                <h5>Assign Challenge
                <button className="btn btn-warning pull-right" onClick={this.replicateToAll} disabled={!uniqueChallenge}>Apply All</button>
                </h5>
                <CandidateTable data={candidates} callback={this.syncCandidateData} challenges={this.props.challenges.challenges}/>
                <div className="submit-candidate">
                <button className="btn btn-info" onClick={this.submitButtonAction} disabled={!enableSubmit}>Submit</button>
                </div>
            </div>
        )
    }

    mayBeRenderError = () => {
        return (
        <div className="text-warning">
            *Something Went wrong, Pleae try submitting again or refresh to upload CSV.
        </div>
        );
    }

    onlyUnique = (value, index, self) => {
        return self.indexOf(value) === index
    }

    replicateToAll = () => {
        const candidates = this.state.candidates;
        const challengeId = candidates.filter(c => c.challengeId)[0].challengeId;
        candidates.map(c => c.challengeId = challengeId);
        this.setState({
            candidates: candidates
        });
    }

    syncCandidateData = (data) => {
        this.setState({
            candidates: data
        });
    }

    submitButtonAction = () => {
        const result = {
            eventId: this.state.eventName,
            candidateChallengeDTOS: this.state.candidates
        }
        this.props.createCandidates(result);
    }

    render() {
        
        if (this.state.shouldRedirect) {
            return (
              <Redirect to={'/home'} />
            );
          }
        return (
            <React.Fragment>
                <div className="row">
                    <div className="col">
                        <h2>Create Event</h2>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        {this.state.currentView === VIEWS.CREATE_EVENT_FORM &&
                        this.renderCandidateCSVUploadForm()
                        }

                        {this.state.currentView === VIEWS.CREATE_EVENT &&
                        this.renderCreateCandidateList()
                        }
                    </div>
                </div>
                <div>
                    {this.state.isRejected === true &&
                    this.mayBeRenderError()
                    }
                </div>
            </React.Fragment>
        )

    }
}

export default CreateCandidate;