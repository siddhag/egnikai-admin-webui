
import createCandidateActionTypes from './CreateCandidateActionTypes';
const createCandidates = (data) => (
    {
        type: createCandidateActionTypes.CREATE_CANDIDATE,
        data: data
    }
);

const createCandidatesPending = () => ({
    type: createCandidateActionTypes.CANDIDATE_CREATE.pending
});

const createCandidatesFulfilled = (data) => ({
    type: createCandidateActionTypes.CANDIDATE_CREATE.fulfilled,
    isCreated: true
});

const createCandidatesRejected = (error) => ({
    type: createCandidateActionTypes.CANDIDATE_CREATE.rejected,
    error: error
});

export {
    createCandidates,
    createCandidatesPending,
    createCandidatesFulfilled,
    createCandidatesRejected
};
