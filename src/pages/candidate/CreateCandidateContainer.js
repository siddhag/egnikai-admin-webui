import {connect} from 'react-redux';
import {fetchChallenges, challengeFetchFulfilled} from '../challenge/challengeAction';
import {createCandidates, createCandidatesRejected} from './CreateCandidateAction';

import CreateCandidate from "./CreateCandidate";

const mapStateToProps = state => ({
    challenges: state.challenges,
    error: state.candidates.error,
    isCreated: state.candidates.createCandidatePromise.isFulfilled,
    isRejected: state.candidates.createCandidatePromise.isRejected
});

const mapDispatchToProps = dispatch => ({
  createCandidates: (data) => {
    dispatch(createCandidates(data));
  },
  fetchChallenges: () => {
    dispatch(fetchChallenges());
  }
  // createCandidatesRejected: () => {
  //   dispatch(createCandidatesRejected());
  // }
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateCandidate);
