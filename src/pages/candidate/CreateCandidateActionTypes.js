const CreateCandidateActionType = {
    CREATE_CANDIDATE: 'Create_candidates',
    CANDIDATE_CREATE: {
        pending: 'CREATE_CANDIDATE/pending',
        fulfilled: 'CREATE_CANDIDATE/fulfilled',
        rejected: 'CREATE_CANDIDATE/rejected'
    }
}
export default CreateCandidateActionType;
