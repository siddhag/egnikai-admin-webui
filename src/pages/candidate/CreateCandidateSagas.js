import { fork, takeLatest, call, put} from  'redux-saga/effects';
import createCandidateActionTypes from './CreateCandidateActionTypes';
import {createCandidatesRejected, createCandidatesFulfilled, createCandidatesPending} from './CreateCandidateAction';
import {apiRequest} from '../../utils/api';


export function* createCandidates(action) {
    console.log(JSON.stringify(action.data));
    try {
        yield put((createCandidatesPending()));
        const result = yield call(apiRequest, {
            url: "candidates/create",
            options: {
                body: JSON.stringify(action.data),
                method: 'POST'
            }
        });
        yield put((createCandidatesFulfilled(result)));
    } catch (err) {
        yield put(createCandidatesRejected(err));
    }
}

export function* watchCreateCandidates() {
    yield takeLatest(createCandidateActionTypes.CREATE_CANDIDATE, createCandidates);
}

export default function* () {
    yield fork(watchCreateCandidates);
}