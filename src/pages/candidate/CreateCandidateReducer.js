import CreateCandidateActionType from './CreateCandidateActionTypes'
import {setPromiseState} from '../../utils/api'

const initialState= {
    isLoading: false,
    isCreated: false,
    createCandidatePromise: setPromiseState()
}

export default (state=initialState, action) => {
    switch (action.type){
        case CreateCandidateActionType.CANDIDATE_CREATE.pending:
          return {...state, createCandidatePromise: setPromiseState(true, false, false)};
        case CreateCandidateActionType.CANDIDATE_CREATE.fulfilled:
          return {...state, createCandidatePromise: setPromiseState(false, true, false), isCreated: action.isCreated};
        case CreateCandidateActionType.CANDIDATE_CREATE.rejected:
          return {...state, createCandidatePromise: setPromiseState(false, false, true)};
        default:
            return state;
    }
}