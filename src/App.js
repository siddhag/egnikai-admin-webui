import React, { Component } from 'react';
import { Switch, Route, NavLink } from "react-router-dom";
import PrivateRoute from './components/private-route/PrivateRoute';
import Home from './pages/home/Home';
import configureStore from './store';
import {Provider} from 'react-redux';
import PastSession from './pages/past-session/PastSession';
import Login from './pages/login/Login';
import { apiRequest } from './utils/api';
import apiConstants from './constants/api';
import { isLoggedIn, logout } from './utils/auth';
import EventEmitter from './EventBus';
import './App.css';
import CreateCandidateContainer from './pages/candidate/CreateCandidateContainer';

let store = configureStore({ });

class App extends Component {  
  state = {
    isUserLoggedIn: isLoggedIn()
  };
  componentWillMount() {
    EventEmitter.on('userLoggedIn', this.updateLoginState);
    EventEmitter.on('userLoggedOut', this.updateLoginState);
  }

  updateLoginState = () => {
    this.setState({
      isUserLoggedIn: isLoggedIn()
    });
  }

  renderSidebar = () => {
    return (
      <nav className="col-md-2 d-none d-md-block bg-light sidebar">
        <div className="sidebar-sticky">
          <ul className="nav flex-column">
            <li className="nav-item">
              <NavLink 
                  className="nav-link" 
                  activeClassName="active"
                  exact
                  to="/home">
                  Active Sessions
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink 
                className="nav-link"
                activeClassName="active"
                exact
                to="/past-sessions">
                  Past Sessions
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink 
                className="nav-link"
                activeClassName="active"
                exact
                to="/create-event">
                  Create Event
              </NavLink>
            </li>
          </ul>           
        </div>
      </nav>
    )
  }

  logoutBtnClick = () => {
    apiRequest({
      url: apiConstants.logout,
      options: {
        method: 'POST'
      }
    }).then(logout);
  }

  render() {
    return (
        <Provider store={store}>

        <React.Fragment>
         <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
          <span className="navbar-brand col-sm-3 col-md-2 mr-0">Egnikai Admin</span>
          <ul className="navbar-nav px-3">
            <li className="nav-item text-nowrap">
              {this.state.isUserLoggedIn ?  
                <a className="nav-link" onClick={this.logoutBtnClick}>Logout</a> :
                <a className="nav-link">Login</a>
                }
            </li>
          </ul>
          </nav>
        <div className="container-fluid">
          <div className="row">
          {this.state.isUserLoggedIn ? this.renderSidebar() : null }

        <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
          <Switch>
            <Route 
                exact 
                path="/"                 
                render={() => <Login isLoggedIn={this.state.isUserLoggedIn} />}
              />
            <PrivateRoute
              exact
              path="/home"
              component={Home}
              isLoggedIn={this.state.isUserLoggedIn}
            />
            <PrivateRoute
              exact
              path="/past-sessions"
              component={PastSession}
              isLoggedIn={this.state.isUserLoggedIn}
            />
            <PrivateRoute
              exact
              path="/create-event"
              component={CreateCandidateContainer}
              isLoggedIn={this.state.isUserLoggedIn}
            />
          </Switch>
        </main>
            
          </div>
        </div>
      </React.Fragment>
        </Provider>

    );
  }
}

export default App;
