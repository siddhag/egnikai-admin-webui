# Egnika Admin Dashboard

Project is divided into 2 folders. 
  - src which is the admin ui
  - mock  which contains all the mock data and the mock server route handling

## Installation
* Install (if you don't have them):
    * [Node.js](http://nodejs.org): `brew install node` on OS X
    * Add plugins and dependencies: `npm install`
* Run:
    * `npm start` — watches the project with continuous rebuild. This will also launch HTTP server with [pushState](https://developer.mozilla.org/en-US/docs/Web/Guide/API/DOM/Manipulating_the_browser_history).
    * `npm run build` — builds minified project for production

This will start the app on http://localhost:3000/