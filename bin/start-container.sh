#!/usr/bin/env bash

. ./bin/setupenv.sh

echo "Cleaning up existing docker images and containers..."
sudo docker stop ${EGNIKAI_ADMIN_UI_CONTAINER}
sudo docker rm ${EGNIKAI_ADMIN_UI_CONTAINER}

echo "Starting web container..."
sudo docker run --name ${EGNIKAI_ADMIN_UI_CONTAINER}  --link ${EGNIKAI_SPRINGBACK_CONTAINER} -it -p 81:81 ${EGNIKAI_ADMIN_UI_IMG}
