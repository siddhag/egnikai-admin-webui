#!/usr/bin/env bash

echo "Creating nginx configuration..."
cp default.conf.template default.conf
sed -i .bk "s/EGNIKAI_ADMIN_UI_CONTAINER/$EGNIKAI_ADMIN_UI_CONTAINER/g" "default.conf"
sed -i .bk "s/EGNIKAI_SPRINGBACK_CONTAINER/$EGNIKAI_SPRINGBACK_CONTAINER/g" "default.conf"
sed -i .bk "s/EGNIKAI_SPRINGBACK_PORT/$EGNIKAI_SPRINGBACK_PORT/g" "default.conf"