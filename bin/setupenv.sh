#!/usr/bin/env bash


EGNIKAI_ADMIN_UI_IMG="egnikai-admin-webui-img"
EGNIKAI_ADMIN_UI_CONTAINER="egnikai-admin-webui-container"
EGNIKAI_SPRINGBACK_CONTAINER="egnikai-app-container"
EGNIKAI_SPRINGBACK_PORT="8080"
