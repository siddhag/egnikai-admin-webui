#!/usr/bin/env bash

set -e
tagName=$1

cfn-create-or-update \
  --stack-name egnikai-admin-ui-${tagName} \
  --template-body file://bin/s3-bucket.yaml \
  --parameters ParameterKey=tagName,ParameterValue=${tagName} \
  --wait

aws s3 sync ./client/build s3://egnikai-admin-ui-${tagName} --delete