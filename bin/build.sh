#!/usr/bin/env bash

. ./bin/setupenv.sh

echo "Cleaning up existing docker images and containers..."

sudo docker stop ${EGNIKAI_ADMIN_UI_CONTAINER}
sudo docker rm ${EGNIKAI_ADMIN_UI_CONTAINER}
sudo docker rmi ${EGNIKAI_ADMIN_UI_IMG}

# Create nginx configurations
. ./bin/tokenise_nginx_conf.sh

echo "Printing modified default.conf"
cat default.conf
echo "Building app..."
npm run initialize
npm run build

echo "Building image..."
sudo docker build -t ${EGNIKAI_ADMIN_UI_IMG} .